/*
	Author	:	Silvio Lazzeretti silvio.lazzeretti@gmail.com
						Giovanni Cotroneo gio.cotroneo@gmail.com
	Data		:	01/06/2017
*/

#ifndef __LEDREADER_H__
#define __LEDREADER_H__

#ifdef __cplusplus
 extern "C" {
#endif
#include "hw.h"	 
	 
void SL_LEDReader_Init(GpioIrqHandler *irqHandler);	 
	 
#ifdef __cplusplus
}
#endif

#endif
