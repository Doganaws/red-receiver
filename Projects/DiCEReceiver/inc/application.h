/*
	Author	:	Silvio Lazzeretti silvio.lazzeretti@gmail.com
						Giovanni Cotroneo gio.cotroneo@gmail.com
	Data		:	01/06/2017
*/

#ifndef __APPLICATION_H
#define __APPLICATION_H

#ifdef __cplusplus
 extern "C" {
#endif
	
#include "utilities.h"
#include <stdint.h>
	 
#define MAX_MEASURE_NUMBER 1
	 
void Application_Init(void);

void MainLoop(void);
		 
void SendMeasures(uint16_t *measures, uint8_t count);
void CheckPowerThresholds(uint32_t power);
	 
#ifdef __cplusplus
 }
#endif

#endif
