# Data output settings	
	# USE_UART1
	Set USE_UART1 Preprocessor Symbol in order to use UART1 as output
	Otherwise the output is setted to the UART2 interface.

	# DEBUG_SIMULATE
	Set DEBUG_SIMULATE Preprocessor Symbol in order to simulate blink
	
# Measurement Packet Definition
	
	+---+-+-+-+-+------------+
	|DDD|P|T|S|C|M(0)..M(C-1)|
	+---+-+-+-+-+------------+
	
	DDD = 3 byte for device address
	P	= 1 byte for packet type (0x00)
	T	= 1 byte for current threshold (0-11)
	S	= 1 byte for sequence number
	C	= 1 byte for counter
	M	= c byte for c measure
	
	Genera il sequente pacchetto terminato con CRLF (simil GPS) in output
	
	$MSGME,DDD_HEX,T,S,RSSI,SNR,C,M(0),...M(C-1)

# Threshold Change Packet Definition

	+---+-+-+
	|DDD|M|V|
	+---+-+-+
	
	DDD = 3 byte for device address
	M	= 1 byte for message type (0x01)
	T	= 1 byte for current threshold (0-11)
	
	Genera il sequente pacchetto terminato con CRLF (simil GPS) in output
	
	$MSGCT,DDD_HEX,T