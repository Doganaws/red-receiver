#include "hw.h"

#include "LEDReader.h"

void SL_LEDReader_Init(GpioIrqHandler *irqHandler)
{
	GPIO_InitTypeDef GPIO_InitStruct;		
			
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING; 

	HW_GPIO_Init(GPIOB,GPIO_PIN_15, &GPIO_InitStruct);
	HW_GPIO_SetIrq(GPIOB, GPIO_PIN_15, 0x00, irqHandler);
}

