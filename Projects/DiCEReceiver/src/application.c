#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "application.h"
#include "stm32l0xx_hal.h"
#include "vcom.h"
#include "radio.h"
#include "utilities.h"

//#define DEBUG_PACKET

extern UART_HandleTypeDef huart2;
extern LPTIM_HandleTypeDef hlptim1;
extern RTC_HandleTypeDef RtcHandle;
	
typedef struct t_ListItem{
		uint8_t buffer[256];
		uint8_t len;
}LIST_ListItem;

#define MAX_ITEMS 10

LIST_ListItem list_items[MAX_ITEMS];
uint8_t list_start = 0;
uint8_t list_count = 0;

RadioEvents_t RadioEvents;

#define RF_FREQUENCY                                868000000 // Hz

#define TX_OUTPUT_POWER                             5        // dBm


#define LORA_BANDWIDTH                              0         // [0: 125 kHz,
                                                              //  1: 250 kHz,
                                                              //  2: 500 kHz,
                                                              //  3: Reserved]
#define LORA_SPREADING_FACTOR                      10         // [SF7..SF12]
#define LORA_CODINGRATE                             1         // [1: 4/5,
                                                              //  2: 4/6,
                                                              //  3: 4/7,
                                                              //  4: 4/8]
#define LORA_PREAMBLE_LENGTH                        8         // Same for Tx and Rx
#define LORA_SYMBOL_TIMEOUT                         5         // Symbols
#define LORA_FIX_LENGTH_PAYLOAD_ON                  false
#define LORA_IQ_INVERSION_ON                        false

typedef enum
{
    LOWPOWER,
    RX,
    RX_TIMEOUT,
    RX_ERROR,
    TX,
    TX_TIMEOUT,
}States_t;

#define RX_TIMEOUT_VALUE                            1000
#define BUFFER_SIZE                                 64 // Define the payload size here

uint16_t BufferSize = BUFFER_SIZE;
uint8_t Buffer[BUFFER_SIZE];

States_t State = LOWPOWER;

int8_t RssiValue = 0;
int8_t SnrValue = 0;

uint32_t TotalBlink = 0;

void OnTxDone( void );

void OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );

void OnTxTimeout( void );

void OnRxTimeout( void );

void OnRxError( void );

void SendThresholdChange(uint8_t value);

void OnLEDBlink(void);

void Application_Init(void)
{
	PRINTF("START\r\n");
	
	RadioEvents.TxDone = OnTxDone;
  RadioEvents.RxDone = OnRxDone;
  RadioEvents.TxTimeout = OnTxTimeout;
  RadioEvents.RxTimeout = OnRxTimeout;
  RadioEvents.RxError = OnRxError;
	  
	Radio.Init( &RadioEvents );

  Radio.SetChannel( RF_FREQUENCY );

  Radio.SetTxConfig( MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
                                 LORA_SPREADING_FACTOR, LORA_CODINGRATE,
                                   LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
                                   true, 0, 0, LORA_IQ_INVERSION_ON, 3000000 );
    
  Radio.SetRxConfig( MODEM_LORA, LORA_BANDWIDTH, LORA_SPREADING_FACTOR,
                                   LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
                                   LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON,
                                   0, true, 0, 0, LORA_IQ_INVERSION_ON, true );

	Radio.Rx(0);
}

void MainLoop(void)
{	
	switch( State )
	{
	case RX:
		{
			Radio.Rx(0);
			State = LOWPOWER;
		}
		break;
	case TX:
		{
		}
		break;
	case RX_TIMEOUT:
	case RX_ERROR:
		{
			State = RX;
		}
		break;
	case TX_TIMEOUT:
		{
			State = LOWPOWER;
		}
		break;
	case LOWPOWER:
		{		
		}
		break;
	}
}

void OnTxDone( void )
{
	Radio.Sleep( );
	State = LOWPOWER;
	//PRINTF("OnTxDone\n");
}

void OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
	Radio.Sleep( );
	BufferSize = size;
	memcpy( Buffer, payload, BufferSize );
	RssiValue = rssi;
	SnrValue = snr;
	State = RX;

	//LED_Toggle(LED_BLUE);
	
	// Fildr Address Devices
	//if(Buffer[0] == 0x00 && Buffer[1] == 0x00 && Buffer[2] == 0x01){
		LED_On(LED3);
	PRINTF("%02X%02X%02X%02X%02X: ", Buffer[0], Buffer[1], Buffer[2], Buffer[3], Buffer[4]);
//#ifdef DEBUG_PACKET
//		PRINTF("RssiValue=%d dBm, SnrValue=%d\r\n", rssi, snr);
//		PRINTF("%02X%02X%02X%02X%02X: ", Buffer[0], Buffer[1], Buffer[2]);
//		PRINTF("%d level\r\n", Buffer[4]);
//		if(Buffer[3] == 0){
//			PRINTF("Sequence Number: %d\r\n", Buffer[5]);
//			for(int i=0;i<Buffer[6];++i){
//				uint32_t val = Buffer[7+i*4] + (((uint32_t)Buffer[7+i*4+1]) << 8)+ (((uint32_t)Buffer[7+i*4+2])<<16)+ (((uint32_t)Buffer[7+i*4+3])<<24);
//				PRINTF("%u ", val);
//			}
//			PRINTF("\r\n");
//		}
//#endif
//		
//		if(Buffer[3] == 0){
//			// Invio il resport di misura
//			PRINTF("$MSGME,%02X%02X%02X,%d,%d,%d,%d,%d",Buffer[0], Buffer[1], Buffer[2],Buffer[4],Buffer[5],rssi,snr,Buffer[6]);
//			for(int i=0;i<Buffer[6];++i){
//				uint32_t val = Buffer[7+i*4] + (((uint32_t)Buffer[7+i*4+1]) << 8)+ (((uint32_t)Buffer[7+i*4+2])<<16)+ (((uint32_t)Buffer[7+i*4+3])<<24);
//				PRINTF(",%u", val);
//			}
//			PRINTF("\r\n");
//		}else if(Buffer[3] == 1){
//			// Invio il report di cambio soglia
//			PRINTF("$MSGCT,%02X%02X%02X,%d\r\n",Buffer[0],Buffer[1],Buffer[2],Buffer[4]);
//		}
		LED_Off(LED3);
		//test commit
	//}
}

void OnTxTimeout( void )
{
	Radio.Sleep( );
	State = TX_TIMEOUT;

//  PRINTF("OnTxTimeout\n");
}

void OnRxTimeout( void )
{
	//Radio.Sleep( );
	State = RX;
//    PRINTF("OnRxTimeout\n");
}

void OnRxError( void )
{
	//Radio.Sleep( );
	State = RX;
	//PRINTF("OnRxError\n");
}

