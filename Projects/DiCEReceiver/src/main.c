/*
	Author	:	Silvio Lazzeretti silvio.lazzeretti@gmail.com
						Giovanni Cotroneo gio.cotroneo@gmail.com
	Data		:	01/06/2017
*/

#include <string.h>
#include "hw.h"
#include "radio.h"
#include "timeServer.h"
#include "delay.h"
#include "low_power.h"
#include "vcom.h"
#include "application.h"

int main( void )
{
  HAL_Init( );
  
  SystemClock_Config( );
  
  DBG_Init( );

  HW_Init( );  

	Application_Init();
	
  while( 1 )
  {
		MainLoop();
  }
}
