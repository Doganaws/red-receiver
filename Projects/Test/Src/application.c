#include "application.h"
#include "stm32l0xx_hal.h"
#include "serialport.h"
#include "radio.h"
#include "utilities.h"
#include "hw_rtc.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

extern UART_HandleTypeDef huart2;
extern LPTIM_HandleTypeDef hlptim1;

typedef struct t_ListItem{
		uint8_t buffer[256];
		uint8_t len;
}LIST_ListItem;

#define MAX_ITEMS 10

LIST_ListItem list_items[MAX_ITEMS];
uint8_t list_start = 0;
uint8_t list_count = 0;

RadioEvents_t RadioEvents;

#define RF_FREQUENCY                                868000000 // Hz

#define TX_OUTPUT_POWER                             5        // dBm


#define LORA_BANDWIDTH                              0         // [0: 125 kHz,
                                                              //  1: 250 kHz,
                                                              //  2: 500 kHz,
                                                              //  3: Reserved]
#define LORA_SPREADING_FACTOR                      10         // [SF7..SF12]
#define LORA_CODINGRATE                             1         // [1: 4/5,
                                                              //  2: 4/6,
                                                              //  3: 4/7,
                                                              //  4: 4/8]
#define LORA_PREAMBLE_LENGTH                        8         // Same for Tx and Rx
#define LORA_SYMBOL_TIMEOUT                         5         // Symbols
#define LORA_FIX_LENGTH_PAYLOAD_ON                  false
#define LORA_IQ_INVERSION_ON                        false

typedef enum
{
    LOWPOWER,
    RX,
    RX_TIMEOUT,
    RX_ERROR,
    TX,
    TX_TIMEOUT,
}States_t;

#define RX_TIMEOUT_VALUE                            1000
#define BUFFER_SIZE                                 64 // Define the payload size here

uint16_t BufferSize = BUFFER_SIZE;
uint8_t Buffer[BUFFER_SIZE];

States_t State = LOWPOWER;

int8_t RssiValue = 0;
int8_t SnrValue = 0;

uint32_t TotalBlink = 0;

uint16_t Measures[MAX_MEASURE_NUMBER];
uint8_t MeasureIndex = 0;

uint16_t Thresholds[] = {250,500,750,1000,1250,1500,1750,2000,2250,2500,2750};
uint8_t CurThreshold = 0;

void OnTxDone( void );

void OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );

void OnTxTimeout( void );

void OnRxTimeout( void );

void OnRxError( void );

void SendThresholdChange(uint8_t value);


void Application_Init(void)
{
	PRINTF("START\r\n");
	
	memset(Measures, 0, sizeof(Measures));
	MeasureIndex = 0;

	HW_RTC_Init();
	
	RadioEvents.TxDone = OnTxDone;
  RadioEvents.RxDone = OnRxDone;
  RadioEvents.TxTimeout = OnTxTimeout;
  RadioEvents.RxTimeout = OnRxTimeout;
  RadioEvents.RxError = OnRxError;
	  
	Radio.Init( &RadioEvents );

  Radio.SetChannel( RF_FREQUENCY );

  Radio.SetTxConfig( MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
                                 LORA_SPREADING_FACTOR, LORA_CODINGRATE,
                                   LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
                                   true, 0, 0, LORA_IQ_INVERSION_ON, 3000000 );
    
  Radio.SetRxConfig( MODEM_LORA, LORA_BANDWIDTH, LORA_SPREADING_FACTOR,
                                   LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
                                   LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON,
                                   0, true, 0, 0, LORA_IQ_INVERSION_ON, true );

	Radio.Sleep( );

	HAL_LPTIM_Counter_Start(&hlptim1, 0xFFFF);
}

void MainLoop(void)
{	
	switch( State )
	{
	case RX:
		{
			State = LOWPOWER;
		}
		break;
	case TX:
		{
		}
		break;
	case RX_TIMEOUT:
	case RX_ERROR:
		{
			State = LOWPOWER;
		}
		break;
	case TX_TIMEOUT:
		{
			State = LOWPOWER;
		}
		break;
	case LOWPOWER:
		{
			if(list_count > 0){
				BACKUP_PRIMASK();
				DISABLE_IRQ();
			
				uint8_t ofs = list_start;
				list_start = (list_start+1)%MAX_ITEMS;
				list_count--;
				
				Radio.Send(list_items[ofs].buffer, list_items[ofs].len);		
				State = TX;
				
				RESTORE_PRIMASK();
				
				LED_Toggle(LED_RED1);
			}
		}
		break;
	}
}

void OnTxDone( void )
{
	Radio.Sleep( );
	State = LOWPOWER;
	LED_Toggle(LED_RED1);
	//PRINTF("OnTxDone\n");
}

void OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
	Radio.Sleep( );
	BufferSize = size;
	memcpy( Buffer, payload, BufferSize );
	RssiValue = rssi;
	SnrValue = snr;
	State = RX;

//   PRINTF("OnRxDone\n");
//   PRINTF("RssiValue=%d dBm, SnrValue=%d\n", rssi, snr);
}

void OnTxTimeout( void )
{
	Radio.Sleep( );
	State = TX_TIMEOUT;

//  PRINTF("OnTxTimeout\n");
}

void OnRxTimeout( void )
{
	Radio.Sleep( );
	State = RX_TIMEOUT;
//    PRINTF("OnRxTimeout\n");
}

void OnRxError( void )
{
	Radio.Sleep( );
	State = RX_ERROR;
	//PRINTF("OnRxError\n");
}

void CheckPowerThresholds(uint32_t power)
{
	PRINTF("Power: %u\r\n", power);

	int i;
	for(i=0; i<11;++i){
		if(Thresholds[i]>power)
			break;
	}
		
	if(i != CurThreshold){
		CurThreshold = i;		
		SendThresholdChange(CurThreshold);
	}
}

void SendThresholdChange(uint8_t value)
{
	assert_param(list_count < MAX_ITEMS);
	
	BACKUP_PRIMASK();
	DISABLE_IRQ();
	
	uint8_t ofs = (list_start+list_count)%MAX_ITEMS;
	
	// Device Address
	list_items[ofs].buffer[0] = 0x00;
	list_items[ofs].buffer[1] = 0x00;
	list_items[ofs].buffer[2] = 0x01;
	
	list_items[ofs].buffer[3] = 1;// MSG Type
	list_items[ofs].buffer[4] = value;
	
	list_items[ofs].len = 5;
	++list_count;
	
	RESTORE_PRIMASK();
}

void SendMeasures(uint16_t *measures, uint8_t count)
{
	assert_param(list_count < MAX_ITEMS);
	
	BACKUP_PRIMASK();
	DISABLE_IRQ();
	
	uint8_t ofs = (list_start+list_count)%MAX_ITEMS;
	
	// Device Address
	list_items[ofs].buffer[0] = 0x00;
	list_items[ofs].buffer[1] = 0x00;
	list_items[ofs].buffer[2] = 0x01;
	
	list_items[ofs].buffer[3] = 0;// MSG Type
	list_items[ofs].buffer[4] = count;
	
	int len = 5;
	for(int i=0; i<count; ++i){
		memcpy(&list_items[ofs].buffer[len], &measures[i], sizeof(measures[i]));
		len += sizeof(measures[i]);
	}
	
	list_items[ofs].len = len;
	++list_count;
	
	RESTORE_PRIMASK();
}
