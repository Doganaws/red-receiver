#include "interrupt_handlers.h"
#include "application.h"
#include "serialport.h"
#include "hw_gpio.h"
#include "radio.h"
#include <string.h>

extern UART_HandleTypeDef huart2;
extern uint32_t TotalBlink;
extern uint16_t Measures[MAX_MEASURE_NUMBER];
extern uint8_t MeasureIndex;
extern LPTIM_HandleTypeDef hlptim1;
extern RTC_HandleTypeDef hrtc;

uint32_t BlinkTick = 0;
uint32_t LastBlinkTick = 0;

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	switch(GPIO_Pin){
		case LEDReader_Pin:
		{	
			OnLEDReaderBlink();			
		}
		break;	
		case RADIO_DIO_0_PORT_Pin:
		case RADIO_DIO_1_PORT_Pin:	
		case RADIO_DIO_3_PORT_Pin:
		{
			HW_GPIO_IrqHandler(GPIO_Pin);
		}
		break;
	}
}

void OnLEDReaderBlink(void)
{
	uint32_t tmpTick = HAL_LPTIM_ReadCounter(&hlptim1);
	if(LastBlinkTick>=tmpTick)
		tmpTick+=0x10000;
	
	BlinkTick += (tmpTick-LastBlinkTick);
	
	if(TotalBlink > 0)
		CheckPowerThresholds(3600*1024/BlinkTick);
	
	LastBlinkTick = tmpTick % 0x10000;
	BlinkTick = 0;
	
	++TotalBlink;
	++Measures[MeasureIndex];
}

void HAL_RTCEx_WakeUpTimerEventCallback(RTC_HandleTypeDef *hrtc)
{
	uint32_t tmpTick = HAL_LPTIM_ReadCounter(&hlptim1);
	if(tmpTick <= LastBlinkTick)
		tmpTick += 0x10000;
	BlinkTick += (tmpTick-LastBlinkTick);
	LastBlinkTick = tmpTick % 0x10000;
	
	if(MeasureIndex == MAX_MEASURE_NUMBER-1)
	{
		BACKUP_PRIMASK();
		DISABLE_IRQ();
		
		SendMeasures(Measures, MeasureIndex+1);
		
		int i;
		PRINTF("Measures: ");
		for(i = 0;i < MAX_MEASURE_NUMBER; ++i){
				PRINTF("%d ", Measures[i]);
		}
		PRINTF("\r\n");
		
		memset(Measures, 0, sizeof(Measures));
		MeasureIndex = 0;
		
		RESTORE_PRIMASK();
	} else{
		MeasureIndex++;
	}
}



