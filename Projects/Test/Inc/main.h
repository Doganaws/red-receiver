/**
  ******************************************************************************
  * File Name          : main.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define RADIO_NSS_PORT_Pin GPIO_PIN_15
#define RADIO_NSS_PORT_GPIO_Port GPIOA
#define RADIO_SCLK_PORT_Pin GPIO_PIN_3
#define RADIO_SCLK_PORT_GPIO_Port GPIOB
#define LED1_PIN_Pin GPIO_PIN_5
#define LED1_PIN_GPIO_Port GPIOB
#define RADIO_TCXO_VCC_PORT_Pin GPIO_PIN_12
#define RADIO_TCXO_VCC_PORT_GPIO_Port GPIOA
#define RADIO_DIO_0_PORT_Pin GPIO_PIN_4
#define RADIO_DIO_0_PORT_GPIO_Port GPIOB
#define LED3_PIN_Pin GPIO_PIN_6
#define LED3_PIN_GPIO_Port GPIOB
#define RADIO_DIO_3_PORT_Pin GPIO_PIN_13
#define RADIO_DIO_3_PORT_GPIO_Port GPIOC
#define LED4_PIN_Pin GPIO_PIN_7
#define LED4_PIN_GPIO_Port GPIOB
#define RADIO_ANT_SWITCH_PORT_TX_BOOST_Pin GPIO_PIN_1
#define RADIO_ANT_SWITCH_PORT_TX_BOOST_GPIO_Port GPIOC
#define RADIO_RESET_PORT_Pin GPIO_PIN_0
#define RADIO_RESET_PORT_GPIO_Port GPIOC
#define RADIO_DIO_1_PORT_Pin GPIO_PIN_1
#define RADIO_DIO_1_PORT_GPIO_Port GPIOB
#define LEDReader_Pin GPIO_PIN_15
#define LEDReader_GPIO_Port GPIOB
#define USER_BUTTON_PIN_Pin GPIO_PIN_2
#define USER_BUTTON_PIN_GPIO_Port GPIOB
#define RADIO_ANT_SWITCH_PORT_RX_Pin GPIO_PIN_1
#define RADIO_ANT_SWITCH_PORT_RX_GPIO_Port GPIOA
#define RADIO_ANT_SWITCH_PORT_TX_RFO_Pin GPIO_PIN_2
#define RADIO_ANT_SWITCH_PORT_TX_RFO_GPIO_Port GPIOC
#define RADIO_MOSI_PORT_Pin GPIO_PIN_7
#define RADIO_MOSI_PORT_GPIO_Port GPIOA
#define BAT_LEVEL_PORT_Pin GPIO_PIN_4
#define BAT_LEVEL_PORT_GPIO_Port GPIOA
#define USARTX_TX_PIN_Pin GPIO_PIN_2
#define USARTX_TX_PIN_GPIO_Port GPIOA
#define RADIO_DIO_2_PORT_Pin GPIO_PIN_0
#define RADIO_DIO_2_PORT_GPIO_Port GPIOB
#define RADIO_MISO_PORT_Pin GPIO_PIN_6
#define RADIO_MISO_PORT_GPIO_Port GPIOA
#define LED2_PIN_Pin GPIO_PIN_5
#define LED2_PIN_GPIO_Port GPIOA
#define USARTX_RX_PIN_Pin GPIO_PIN_3
#define USARTX_RX_PIN_GPIO_Port GPIOA

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
