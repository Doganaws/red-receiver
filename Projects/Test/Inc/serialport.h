#ifndef __SERIALPORT_H__
#define __SERIALPORT_H__

#ifdef __cplusplus
 extern "C" {
#endif
    
/** 
* @brief  Records string on circular Buffer and set SW interrupt
* @note   Set NVIC to call vcom_Send
* @param  string
* @return None
*/
void vcom_Send( char *format, ... );

/** 
* @brief  Sends circular Buffer on com port in IT mode
* @note   called from low Priority interrupt
* @param  None
* @return None
*/
void vcom_Print( void);

/** 
* @brief  Records string on circular Buffer
* @note   To be called only from critical section from low power section
*         Other wise use vcom_Send
* @param  string
* @return None
*/
void vcom_Send_Lp( char *format, ... );

/* Exported macros -----------------------------------------------------------*/
#if 1
#define PRINTF(...)            vcom_Send(__VA_ARGS__)
#else
#define PRINTF(...)
#endif


#ifdef __cplusplus
}
#endif

#endif 
