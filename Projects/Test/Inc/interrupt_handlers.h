#ifndef __INTERRUPT_HANDLERS_H
#define __INTERRUPT_HANDLERS_H

#ifdef __cplusplus
 extern "C" {
#endif

#include "main.h"
#include "stm32l0xx_hal.h"
#include <stdint.h>
	 
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);
void OnLEDReaderBlink(void);
void HAL_RTCEx_WakeUpTimerEventCallback(RTC_HandleTypeDef *hrtc);
	 
#ifdef __cplusplus
 }
#endif

#endif
